from collections import Counter

data = open('day2_data', 'r')
data_contents = data.readlines()


c = [0, 0]
for i in data_contents:
    a = [j for i,j in Counter(i).most_common()]
    if 3 in a:
        c[0] += 1
    if 2 in a:
         c[1] += 1
print(c[0] * c[1])











